from PyQt4 import QtCore, QtGui
from DBGUI import Ui_MainWindow
import sys
import time
import Main

class DBGUIController(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.parameters=[None]*12
        self.totalWeight=0
        self.prevRead=0
        self.prevWrite=0
        self.prevUpdate=0
        self.prevJoin=0
        self.prevBin=0
        self.ready=0
        self.main=None
        self.binaryTranSpinner.setMaximum(100)
        #For Text Fields
        self.dbNameEntry.textChanged[str].connect(self.onDBNChanged)
        self.unEntry.textChanged[str].connect(self.onUNChanged)
        self.pwEntry.textChanged[str].connect(self.onPWChanged)
        self.driverEntry.textChanged[str].connect(self.onDriverChanged)
        self.tranLogLocationBrowser.textChanged[str].connect(self.onTranLogChanged)
        self.threadLogLocationBrowser.textChanged[str].connect(self.onThreadLogChanged)
        
        #For Weights
        #self.weightType.valueChanged.connect(self.weightType_changed)
        self.numThreadSpinner.valueChanged.connect(self.numThreadsChanged)
        self.readWeightSpinner.valueChanged.connect(self.readWeightChanged)
        self.writeWeightSpiner.valueChanged.connect(self.writeWeightChanged)
        self.updateWeightSpinner.valueChanged.connect(self.updateWeightChanged)
        self.joinWeightSpinner.valueChanged.connect(self.joinWeightChanged)
        self.binaryTranSpinner.valueChanged.connect(self.binWeightChanged)
        
        #Need to set it up for each weight, will track whenever value is changed.
            
        #For Buttons
        #self.buttonName.clicked.connect(self.buttonType_clicked)
        self.runTestButton.setEnabled(0)
        self.runTestButton.clicked.connect(self.runButtonClicked)
        self.clearAllButton.clicked.connect(self.clearButtonClicked)
        self.stopTestButton.clicked.connect(self.stopButtonClicked)
        
        self.actionClear_All.triggered.connect(self.clearButtonClicked)
        self.actionRun_Test.triggered.connect(self.runButtonClicked)
        #For Browse Buttons
        self.tranLogLocationBrowserButton.clicked.connect(self.tranLogButtonClicked)
        self.threadLogLocationBrowserButton.clicked.connect(self.threadLogButtonClicked)
        #For Top Menu Items
        #self.actionDo_Something.triggered.connect(self.buttonType_clicked)
        self.actionQuit_2.triggered.connect(self.quitAction_clicked)

    def onDBNChanged(self, text):
        self.parameters[0]=text
        if text=='':
            self.runTestButton.setEnabled(0);
        if text!='':
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0;
            if self.ready==1:
                self.runTestButton.setEnabled(1)
            else:
                self.runTestButton.setEnabled(0)
    def onUNChanged(self, text):
        self.parameters[1]=text
        if text=='':
            self.runTestButton.setEnabled(0);
        if text!='':
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0;
            if self.ready==1:
                self.runTestButton.setEnabled(1)
            else:
                self.runTestButton.setEnabled(0)
    def onPWChanged(self, text):
        self.parameters[2]=text
        if text=='':
            self.runTestButton.setEnabled(0);
        if text!='':
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0;
            if self.ready==1:
                self.runTestButton.setEnabled(1)
            else:
                self.runTestButton.setEnabled(0)
    def onDriverChanged(self, text):
        self.parameters[3]=text
        if text=='':
            self.runTestButton.setEnabled(0);
        if text!='':
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0;
            if self.ready==1:
                self.runTestButton.setEnabled(1)
            else:
                self.runTestButton.setEnabled(0)
    def numThreadsChanged(self):
        self.parameters[4]=self.numThreadSpinner.value()
        if self.numThreadSpinner.value()==0:
            self.runTestButton.setEnabled(0)
        if self.numThreadSpinner.value()!=0:
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0;
            if self.ready==1:
                self.runTestButton.setEnabled(1)
    def readWeightChanged(self):
        self.totalWeight-=self.prevRead
        self.prevRead=self.readWeightSpinner.value()
        self.totalWeight+=self.readWeightSpinner.value()
        self.totalUsedBar.setValue(self.totalWeight)
        if self.totalWeight>=100:
            if self.writeWeightSpiner.value()==0:
                self.writeWeightSpiner.setEnabled(0)
            if self.updateWeightSpinner.value()==0:
                self.updateWeightSpinner.setEnabled(0)
            if self.joinWeightSpinner.value()==0:
                self.joinWeightSpinner.setEnabled(0)
            if self.binaryTranSpinner.value()==0:
                self.binaryTranSpinner.setEnabled(0)
        elif self.totalWeight<100:
            if self.writeWeightSpiner.value()==0:
                self.writeWeightSpiner.setEnabled(1)
            if self.updateWeightSpinner.value()==0:
                self.updateWeightSpinner.setEnabled(1)
            if self.joinWeightSpinner.value()==0:
                self.joinWeightSpinner.setEnabled(1)
            if self.binaryTranSpinner.value()==0:
                self.binaryTranSpinner.setEnabled(1)
            self.runTestButton.setEnabled(0)
        self.writeWeightSpiner.setMaximum(100-self.totalWeight+self.writeWeightSpiner.value())
        self.updateWeightSpinner.setMaximum(100-self.totalWeight+self.updateWeightSpinner.value())
        self.joinWeightSpinner.setMaximum(100-self.totalWeight+self.joinWeightSpinner.value())
        self.binaryTranSpinner.setMaximum(101-self.totalWeight+self.binaryTranSpinner.value())
        if self.totalWeight==100:
            self.parameters[5]=self.readWeightSpinner.value()
            self.parameters[6]=self.writeWeightSpiner.value()
            self.parameters[7]=self.updateWeightSpinner.value()
            self.parameters[8]=self.joinWeightSpinner.value()
            self.parameters[9]=self.binaryTranSpinner.value()
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0;
            if self.ready==1:
                self.runTestButton.setEnabled(1)
    def writeWeightChanged(self):
        self.totalWeight-=self.prevWrite
        self.prevWrite=self.writeWeightSpiner.value()
        self.totalWeight+=self.writeWeightSpiner.value()
        self.totalUsedBar.setValue(self.totalWeight)
        if self.totalWeight>=100:
            if self.readWeightSpinner.value()==0:
                self.readWeightSpinner.setEnabled(0)
            if self.updateWeightSpinner.value()==0:
                self.updateWeightSpinner.setEnabled(0)
            if self.joinWeightSpinner.value()==0:
                self.joinWeightSpinner.setEnabled(0)
            if self.binaryTranSpinner.value()==0:
                self.binaryTranSpinner.setEnabled(0)
        elif self.totalWeight<100:
            if self.readWeightSpinner.value()==0:
                self.readWeightSpinner.setEnabled(1)
            if self.updateWeightSpinner.value()==0:
                self.updateWeightSpinner.setEnabled(1)
            if self.joinWeightSpinner.value()==0:
                self.joinWeightSpinner.setEnabled(1)
            if self.binaryTranSpinner.value()==0:
                self.binaryTranSpinner.setEnabled(1)
            self.runTestButton.setEnabled(0)
        self.readWeightSpinner.setMaximum(100-self.totalWeight+self.readWeightSpinner.value())
        self.updateWeightSpinner.setMaximum(100-self.totalWeight+self.updateWeightSpinner.value())
        self.joinWeightSpinner.setMaximum(100-self.totalWeight+self.joinWeightSpinner.value())
        self.binaryTranSpinner.setMaximum(100-self.totalWeight+self.binaryTranSpinner.value())
        if self.totalWeight==100:
            self.parameters[5]=self.readWeightSpinner.value()
            self.parameters[6]=self.writeWeightSpiner.value()
            self.parameters[7]=self.updateWeightSpinner.value()
            self.parameters[8]=self.joinWeightSpinner.value()
            self.parameters[9]=self.binaryTranSpinner.value()
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0;
            if self.ready==1:
                self.runTestButton.setEnabled(1)
    def updateWeightChanged(self):
        self.totalWeight-=self.prevUpdate
        self.prevUpdate=self.updateWeightSpinner.value()
        self.totalWeight+=self.updateWeightSpinner.value()
        self.totalUsedBar.setValue(self.totalWeight)
        if self.totalWeight>=100:
            if self.readWeightSpinner.value()==0:
                self.readWeightSpinner.setEnabled(0)
            if self.writeWeightSpiner.value()==0:
                self.writeWeightSpiner.setEnabled(0)
            if self.joinWeightSpinner.value()==0:
                self.joinWeightSpinner.setEnabled(0)
            if self.binaryTranSpinner.value()==0:
                self.binaryTranSpinner.setEnabled(0)
        elif self.totalWeight<100:
            if self.readWeightSpinner.value()==0:
                self.readWeightSpinner.setEnabled(1)
            if self.writeWeightSpiner.value()==0:
                self.writeWeightSpiner.setEnabled(1)
            if self.joinWeightSpinner.value()==0:
                self.joinWeightSpinner.setEnabled(1)
            if self.binaryTranSpinner.value()==0:
                self.binaryTranSpinner.setEnabled(1)
            self.runTestButton.setEnabled(0)
        self.writeWeightSpiner.setMaximum(100-self.totalWeight+self.writeWeightSpiner.value())
        self.readWeightSpinner.setMaximum(100-self.totalWeight+self.readWeightSpinner.value())
        self.joinWeightSpinner.setMaximum(100-self.totalWeight+self.joinWeightSpinner.value())
        self.binaryTranSpinner.setMaximum(100-self.totalWeight+self.binaryTranSpinner.value())
        if self.totalWeight==100:
            self.parameters[5]=self.readWeightSpinner.value()
            self.parameters[6]=self.writeWeightSpiner.value()
            self.parameters[7]=self.updateWeightSpinner.value()
            self.parameters[8]=self.joinWeightSpinner.value()
            self.parameters[9]=self.binaryTranSpinner.value()  
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0;
            if self.ready==1:
                self.runTestButton.setEnabled(1)  
    def joinWeightChanged(self):
        self.totalWeight-=self.prevJoin
        self.prevJoin=self.joinWeightSpinner.value()
        self.totalWeight+=self.joinWeightSpinner.value()
        self.totalUsedBar.setValue(self.totalWeight)
        if self.totalWeight>=100:
            if self.writeWeightSpiner.value()==0:
                self.writeWeightSpiner.setEnabled(0)
            if self.updateWeightSpinner.value()==0:
                self.updateWeightSpinner.setEnabled(0)
            if self.readWeightSpinner.value()==0:
                self.readWeightSpinner.setEnabled(0)
            if self.binaryTranSpinner.value()==0:
                self.binaryTranSpinner.setEnabled(0)
        elif self.totalWeight<100:
            if self.writeWeightSpiner.value()==0:
                self.writeWeightSpiner.setEnabled(1)
            if self.updateWeightSpinner.value()==0:
                self.updateWeightSpinner.setEnabled(1)
            if self.readWeightSpinner.value()==0:
                self.readWeightSpinner.setEnabled(1)
            if self.binaryTranSpinner.value()==0:
                self.binaryTranSpinner.setEnabled(1)
            self.runTestButton.setEnabled(0)
        self.writeWeightSpiner.setMaximum(100-self.totalWeight+self.writeWeightSpiner.value())
        self.updateWeightSpinner.setMaximum(100-self.totalWeight+self.updateWeightSpinner.value())
        self.readWeightSpinner.setMaximum(100-self.totalWeight+self.readWeightSpinner.value())
        self.binaryTranSpinner.setMaximum(100-self.totalWeight+self.binaryTranSpinner.value())
        if self.totalWeight==100:
            self.parameters[5]=self.readWeightSpinner.value()
            self.parameters[6]=self.writeWeightSpiner.value()
            self.parameters[7]=self.updateWeightSpinner.value()
            self.parameters[8]=self.joinWeightSpinner.value()
            self.parameters[9]=self.binaryTranSpinner.value()
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0;
            if self.ready==1:
                self.runTestButton.setEnabled(1)
    def binWeightChanged(self):
        self.totalWeight-=self.prevBin
        self.prevBin=self.binaryTranSpinner.value()
        self.totalWeight+=self.binaryTranSpinner.value()
        self.totalUsedBar.setValue(self.totalWeight)
        if self.totalWeight>=100:
            if self.writeWeightSpiner.value()==0:
                self.writeWeightSpiner.setEnabled(0)
            if self.updateWeightSpinner.value()==0:
                self.updateWeightSpinner.setEnabled(0)
            if self.joinWeightSpinner.value()==0:
                self.joinWeightSpinner.setEnabled(0)
            if self.readWeightSpinner.value()==0:
                self.readWeightSpinner.setEnabled(0)
        elif self.totalWeight<100:
            if self.writeWeightSpiner.value()==0:
                self.writeWeightSpiner.setEnabled(1)
            if self.updateWeightSpinner.value()==0:
                self.updateWeightSpinner.setEnabled(1)
            if self.joinWeightSpinner.value()==0:
                self.joinWeightSpinner.setEnabled(1)
            if self.readWeightSpinner.value()==0:
                self.readWeightSpinner.setEnabled(1)
            self.runTestButton.setEnabled(0)
        self.writeWeightSpiner.setMaximum(100-self.totalWeight+self.writeWeightSpiner.value())
        self.updateWeightSpinner.setMaximum(100-self.totalWeight+self.updateWeightSpinner.value())
        self.joinWeightSpinner.setMaximum(100-self.totalWeight+self.joinWeightSpinner.value())
        self.readWeightSpinner.setMaximum(100-self.totalWeight+self.readWeightSpinner.value())
        if self.totalWeight==100:
            self.parameters[5]=self.readWeightSpinner.value()
            self.parameters[6]=self.writeWeightSpiner.value()
            self.parameters[7]=self.updateWeightSpinner.value()
            self.parameters[8]=self.joinWeightSpinner.value()
            self.parameters[9]=self.binaryTranSpinner.value()
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0;
            if self.ready==1:
                self.runTestButton.setEnabled(1)
    def quitAction_clicked(self):
        self.close()
    def tranLogButtonClicked(self):
        self.tranLogLocationBrowser.setText(QtGui.QFileDialog.getOpenFileName())
    def onTranLogChanged(self, text):
        self.parameters[10]=text;
        if text=='':
            self.runTestButton.setEnabled(0)
        if text!='':
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0
            if self.ready==1:
                self.runTestButton.setEnabled(1)
    def threadLogButtonClicked(self):
        self.threadLogLocationBrowser.setText(QtGui.QFileDialog.getOpenFileName())
    def onThreadLogChanged(self, text):
        self.parameters[11]=text;
        if text=='':
            self.runTestButton.setEnabled(0)
        if text!='':
            self.ready=1
            for x in self.parameters:
                if x==None:
                    self.ready=0
            if self.ready==1:
                self.runTestButton.setEnabled(1)
    def runButtonClicked(self):
        self.testStatusChangeLabel.setText("Test Running")
        self.stopTestButton.setEnabled(1)
        time.sleep(5)
        self.main=Main.mainClass(self.parameters)
        self.main.run()
    def clearButtonClicked(self):
        self.dbNameEntry.clear()
        self.unEntry.clear()
        self.pwEntry.clear()
        self.driverEntry.clear()
        self.numThreadSpinner.setValue(0)
        self.readWeightSpinner.setValue(0)
        self.writeWeightSpiner.setValue(0)
        self.updateWeightSpinner.setValue(0)
        self.joinWeightSpinner.setValue(0)
        self.binaryTranSpinner.setValue(0)
        self.tranLogLocationBrowser.clear()
        self.threadLogLocationBrowser.clear()
        self.testStatusChangeLabel.setText("")
    def stopButtonClicked(self):
        self.main.stopTest()
if __name__=='__main__':
    app=QtGui.QApplication(sys.argv)
    MainApp=DBGUIController()
    MainApp.show()
    sys.exit(app.exec_())
            