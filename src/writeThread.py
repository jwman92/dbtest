'''
Created on Jun 2, 2014

@author: jwaldman
'''
import threading
import time
import datetime
import pypyodbc
import string
import random
class writeThread(threading.Thread):
    def __init__(self,threadID, name, counter, parameters):
        threading.Thread.__init__(self)
        self.counter=counter
        self.threadID=threadID
        self.name=name
        self.dbName=parameters[0]
        self.uName=parameters[1]
        self.password=parameters[2]
        self.driverName=parameters[3]
        self.logFile=parameters[4]
        self.threadLog=parameters[5]
    def run(self):
        result_file=open(str(self.threadLog), "a")
        result_file.write("Write Thread #:"+str(self.counter)+" starts @"+str(get_timestamp()+"\n"))
        log_file=open(str(self.logFile), "a")
        
        conn_str1="Driver={"+str(self.driverName)+"}; "
        conn_str2="TRUSTED_CONNECTION=YES; "
        conn_str3="SERVER="+str(self.dbName)+"; "
        conn_str4=" DATABASE=AdventureWorks2012;"
        conn_str5=" UID="+str(self.uName)+"; "
        conn_str6=" PWD="+str(self.password)+";"
        conn_str=conn_str1+conn_str2+conn_str3+conn_str4+conn_str5+conn_str6
        try:
            conn = pypyodbc.connect(conn_str)
        except pypyodbc.DatabaseError:
            print("Connection to Database has Failed!!")
        cursor=conn.cursor()
        #Generate Command
        firstName=genRandomFN(cursor)
        firstName=firstName[2:-3]
        lastName=genRandomLN(cursor)
        lastName=lastName[2:-3]
                #Execute Command
        str1="INSERT INTO TestDB"
        str2=" (FirstName, LastName) VALUES("
        str3="'"+str(firstName)+"', '"
        str4=str(lastName)+"');"
        command=str1+str2+str3+str4
        try:
            cursor.execute(command)
        except pypyodbc.ProgrammingError:
            print("Error inserting into AdventureWorks DB!!")
        cursor.commit()
        cursor.close()
        conn.close()
        #Log Action
        log_file.write("Thread "+str(self.counter)+ " writes: "+str(firstName)+ " "+str(lastName)+"\n")
        result_file.write("Write Thread #:"+str(self.counter)+" completes @"+str(get_timestamp())+"\n")
        result_file.close()
        log_file.close()
def get_timestamp():
    ts=time.time()
    st=datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    return st

def genRandomFN(cursor):
    command="SELECT FirstName FROM Person.Person"
    cursor.execute(command)
    count=0
    rand=random.randint(0, 10000)   
    for row in cursor:
        if(count==rand):
            return str(row)
        count+=1

def genRandomLN(cursor):
    command="SELECT LastName FROM Person.Person"
    cursor.execute(command)
    count=0
    rand=random.randint(0, 10000)
    for row in cursor:
        if(count==rand):
            return str(row)
        count+=1


        
        
