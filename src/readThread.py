'''
Created on Jun 2, 2014

@author: jwaldman
'''
import threading
import time
import datetime
import pypyodbc
import string
import random
class readThread(threading.Thread):
    def __init__(self, threadID, name, counter, parameters):
        threading.Thread.__init__(self)
        self.counter=counter
        self.name=name
        self.threadID=threadID
        self.dbName=parameters[0]
        self.uName=parameters[1]
        self.password=parameters[2]
        self.driverName=parameters[3]
        self.logFile=parameters[4]
        self.threadLog=parameters[5]
    def run(self):
        #Note in file when thread started
        result_file=open(str(self.threadLog), "a")
        result_file.write("Read Thread #:"+str(self.counter)+" starts @"+str(get_timestamp()+"\n"))
        log_file=open(str(self.logFile), "a")
        
        conn_str1="Driver={"+str(self.driverName)+"}; "
        conn_str2="TRUSTED_CONNECTION=YES; "
        conn_str3="SERVER="+str(self.dbName)+"; "
        conn_str4=" DATABASE=AdventureWorks2012;"
        conn_str5=" UID="+str(self.uName)+"; "
        conn_str6=" PWD="+str(self.password)+";"
        conn_str=conn_str1+conn_str2+conn_str3+conn_str4+conn_str5+conn_str6
        try:
            conn = pypyodbc.connect(conn_str)
        except pypyodbc.DatabaseError:
            print("Connection to Database has Failed!!")  
        cursor=conn.cursor()      
        #Generate Command 
        str1="SELECT FirstName, LastName FROM TestDB "
        str2="WHERE LastName LIKE '"
        str3=str(getRandomChar())+"%'"
        command=str1+str2+str3;
        
        #Read From Table
        try:
            cursor.execute(command)
        except pypyodbc.ProgrammingError:
            print("Error reading tables in AdventureWorks DB!!")
    
        rows=cursor.fetchall()
        #Write to test file
        for row in rows:
            log_file.write("Thread "+str(self.counter)+" reads: "+ str(row) +"\n")
        cursor.close()
        conn.close()
        result_file.write("Read Thread #:"+str(self.counter)+" completes @"+str(get_timestamp())+"\n")
        log_file.close()
        result_file.close()
def get_timestamp():
    ts=time.time()
    st=datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    return st

def getRandomChar():
    return random.choice(string.ascii_uppercase)    
