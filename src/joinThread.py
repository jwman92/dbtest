'''
Created on Jun 3, 2014

@author: jwaldman
'''
import threading
import time
import datetime
import pypyodbc
import random
class joinThread(threading.Thread):
    def __init__(self, threadID, name, counter, parameters):
        threading.Thread.__init__(self)
        self.counter=counter
        self.name=name
        self.threadID=threadID
        self.dbName=parameters[0]
        self.uName=parameters[1]
        self.password=parameters[2]
        self.driverName=parameters[3]
        self.logFile=parameters[4]
        self.threadLog=parameters[5]
    def run(self):
        #Set up Logging
        result_file=open(str(self.threadLog), "a")
        result_file.write("Join Thread #: "+str(self.counter)+" started @"+str(get_timestamp())+"\n")
        log_file=open(str(self.logFile), "a")
        
        #Connect to database
        conn_str1="Driver={"+str(self.driverName)+"}; "
        conn_str2="TRUSTED_CONNECTION=YES; "
        conn_str3="SERVER="+str(self.dbName)+"; "
        conn_str4=" DATABASE=AdventureWorks2012;"
        conn_str5=" UID="+str(self.uName)+"; "
        conn_str6=" PWD="+str(self.password)+";"
        conn_str=conn_str1+conn_str2+conn_str3+conn_str4+conn_str5+conn_str6
        
        try:
            conn=pypyodbc.connect(conn_str)
        except:
            print("Connection to Database has Failed!!")
        cursor=conn.cursor()
        cursor.execute("DROP TABLE joinTest")
        cursor.commit()
        randomID=random.randint(1, 10000)
        str1="SELECT ID, FirstName, LastName INTO joinTest "
        str2="FROM dbo.TestDB WHERE ID="+str(randomID)+";"
        command=str1+str2
        cursor.execute(command)
        cursor.commit()
        #Set up and execute join command
        str1="SELECT joinTest.ID, joinTest.FirstName, joinTest.LastName, documentTable.Document"
        str2=" FROM joinTest"
        str3=" INNER JOIN documentTable"
        str4=" ON joinTest.ID=documentTable.ID;"
        command=str1+str2+str3+str4
        cursor.execute(command)
        rows=cursor.fetchall()
        #Write to log file
        for row in rows:
            log_file.write("Thread "+str(self.counter)+" returns a result from a join statement \n")
        #Finishing items
        cursor.close()
        conn.close()
        result_file.write("Join Thread #:"+str(self.counter)+" completes @"+str(get_timestamp())+"\n")
        log_file.close()
        result_file.close()
    
    
def get_timestamp():
    ts=time.time()
    st=datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    return st