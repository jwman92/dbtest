'''
Created on Jun 3, 2014

@author: jwaldman
'''
import threading
import datetime
import time
import pypyodbc
import random
import binascii
filePath=['G:\Image1.png', 'G:\Image2.png', 'G:\Image3.png', 'G:\Image4.png', 'G:\Image5.png']
def getRandomFile():
    fileNum=random.randint(0, 4)
    return filePath[fileNum]
class fileThread(threading.Thread):
    def __init__(self, threadID, name, counter, parameters):
        threading.Thread.__init__(self)
        self.counter=counter
        self.threadID=threadID
        self.name=name
        self.dbName=parameters[0]
        self.uName=parameters[1]
        self.password=parameters[2]
        self.driverName=parameters[3]
        self.logFile=parameters[4]
        self.threadLog=parameters[5]
    def run(self):
        result_file=open(str(self.threadLog), "a")
        result_file.write("Binary Thread #:"+str(self.counter)+" starts @"+str(get_timestamp()+"\n"))
        log_file=open(str(self.logFile), "a")
        
        conn_str1="Driver={"+str(self.driverName)+"}; "
        conn_str2="TRUSTED_CONNECTION=YES; "
        conn_str3="SERVER="+str(self.dbName)+"; "
        conn_str4=" DATABASE=AdventureWorks2012;"
        conn_str5=" UID="+str(self.uName)+"; "
        conn_str6=" PWD="+str(self.password)+";"
        conn_str=conn_str1+conn_str2+conn_str3+conn_str4+conn_str5+conn_str6
        try:
            conn = pypyodbc.connect(conn_str)
        except pypyodbc.DatabaseError:
            print("Connection to Database has Failed!!")
        cursor=conn.cursor()
        
        #Do Work
        random_read(cursor, result_file, self.counter)
        cursor.close()
        conn.close()
        result_file.write("Binary Thread #:"+str(self.counter)+" completes @"+str(get_timestamp())+"\n")
        result_file.close()
        log_file.close()

def get_timestamp():
    ts=time.time()
    st=datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    return st

def random_read(cursor, result_file, counter):
    IDNum=random.randint(1, 10000)
    str1="SELECT Document FROM documentTable "
    str2="WHERE ID="+str(IDNum)
    cursor.execute(str1+str2)
    imageText=cursor.fetchone()
    imageText=str(imageText)[11:-3]
    CRC=binascii.crc32(str.encode(str(imageText)))
    str3="SELECT CRCCode FROM crcTable "
    str4="WHERE ID='"+str(IDNum)+"';"
    cursor.execute(str3+str4)
    imageCRC=cursor.fetchone()
    imageCRC=str(imageCRC)[2:-3]
    if(str(CRC)==str(imageCRC)):
        result_file.write("Binary Thread #:"+str(counter)+" passes CRC Check \n")
    
def random_update(log_file, counter):
    fileName=getRandomFile()
    ID=random.randint(1, 10000)
    str1="UPDATE documentTable "
    str2="SET Document= * FROM OPENROWSET(BULK N'"
    str3=str(fileName)+"', SINGLE_BLOB) as imagedata "
    str4="WHERE ID="+str(ID)
    log_file.write("Thread "+str(counter)+" updates document for ID: "+str(ID))
    return str(str1)+str(str2)+str(str3)+str(str4)

