'''
Created on Jun 2, 2014

@author: jwaldman
'''
import fileThread
import joinThread
import writeThread
import readThread
import updateThread
import fileSetup
import InitialSetup
import random
import datetime
import time
def get_timestamp():
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    return st

class mainClass():
    def __init__(self, parameters):
        self.threadParams=[parameters[0], parameters[1], parameters[2], parameters[3], parameters[10], parameters[11]]
        self.threadCount=parameters[4]
        self.weightRead=parameters[5]
        self.weightWrite=parameters[6]
        self.weightUpdate=parameters[7]
        self.weightJoin=parameters[8]
        self.weightBinTran=parameters[9]
        self.logFile=parameters[10]
        self.threadLogFile=parameters[11]
        self.toStop=1
    def run(self):
        iSetup=InitialSetup.InitialSetup(self.threadParams)
        iSetup.run()
        #fSetup=fileSetup.fileSetup(self.threadParams)
        #fSetup.run()
        result_file = open(str(self.threadLogFile), "w")
        result_file.write("Wake County Test @" + str(get_timestamp())+"\n")
        log_file=open(str(self.logFile), "w")
        log_file.write("Wake County Log @"+ str(get_timestamp())+"\n")
        result_file.close()
        log_file.close()
        count=0
        while self.toStop==1:
            threadPool=list()
            for x in range(0, self.threadCount):
                rand=random.randint(1,100)
                if rand<=self.weightRead:
                    thread=readThread.readThread(count, "Thread"+str(count), count, self.threadParams)
                    threadPool.append(thread)
                elif rand<=self.weightRead+self.weightWrite:
                    thread=writeThread.writeThread(count, "Thread "+str(count), count, self.threadParams)
                    threadPool.append(thread)
                elif rand<=self.weightRead+self.weightWrite+self.weightUpdate:    
                    thread=updateThread.updateThread(count, "Thread "+str(count), count, self.threadParams)
                    threadPool.append(thread)
                elif rand<=self.weightRead+self.weightWrite+self.weightUpdate+self.weightJoin:
                    thread=joinThread.joinThread(count, "Thread "+str(count), count, self.threadParams)
                    threadPool.append(thread)
                elif rand<=100:
                    thread=fileThread.fileThread(count, "Thread "+str(count), count, self.threadParams)
                    threadPool.append(thread)
                count=count+1
            for thread in threadPool:
                thread.start()
            time.sleep(2)
        #Stop Button Pressed, Can Exit
    def stopTest(self):
        self.toStop=0








