'''
Created on Jun 3, 2014

@author: jwaldman
'''
import pypyodbc
import random
import binascii
filePath=['G:\Image1.jpg', 'G:\Image2.jpg', 'G:\Image3.jpg', 'G:\Image4.jpg', 'G:\Image5.jpg']
def getRandomFile():
    fileNum=random.randint(0, 4)
    return filePath[fileNum]
class fileSetup():
    def __init__(self, parameters):
        self.dbName=parameters[0]
        self.uName=parameters[1]
        self.password=parameters[2]
        self.driverName=parameters[3]
    def run(self):
        conn_str1="Driver={"+str(self.driverName)+"}; "
        conn_str2="TRUSTED_CONNECTION=YES; "
        conn_str3="SERVER="+str(self.dbName)+"; "
        conn_str4=" DATABASE=AdventureWorks2012;"
        conn_str5=" UID="+str(self.uName)+"; "
        conn_str6=" PWD="+str(self.password)+";"
        conn_str=conn_str1+conn_str2+conn_str3+conn_str4+conn_str5+conn_str6
        
        try:
            conn=pypyodbc.connect(conn_str)
        except pypyodbc.DatabaseError:
            print("Connection to Database has Failed!!")
        cursor=conn.cursor()
        cursor.execute("DROP TABLE documentTable")
        cursor.commit()
        cursor.execute("""CREATE TABLE documentTable(ID int IDENTITY(1,1) PRIMARY KEY, Document varbinary(max));""")
        cursor.commit()
        cursor.execute("""DROP TABLE crcTable""")
        cursor.commit()
        cursor.execute(""" CREATE TABLE crcTable(ID int IDENTITY(1,1) PRIMARY KEY, CRCCode varchar(30));""")
        cursor.commit()
        cursor.execute("""SELECT ID from TestDB;""")
        rows=cursor.fetchall()
        for row in rows:
            #Randomly Select File
            fileName=getRandomFile()
            imgText=open(fileName, 'rb')
            imgTextStr=imgText.read()
            imgText.close()
            comm="INSERT INTO crcTable(CRCCode) VALUES('"+str(binascii.crc32(str.encode(str(imgTextStr))))+"');"
            #print(comm)
            cursor.execute(comm)
            cursor.commit()
            str1="INSERT INTO documentTable(Document) SELECT * FROM OPENROWSET(BULK N'"
            str2=str(fileName)+"', SINGLE_BLOB) as imagedata;"
            command=str1+str2
            cursor.execute(command)
            cursor.commit()
    
    

    