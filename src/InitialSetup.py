'''
Created on Jun 2, 2014

@author: jwaldman
'''
import pypyodbc
class InitialSetup():
    def __init__(self, parameters):
        self.dbName=parameters[0]
        self.uName=parameters[1]
        self.password=parameters[2]
        self.driverName=parameters[3]
    def run(self):
        conn_str1="Driver={"+str(self.driverName)+"}; "
        conn_str2="TRUSTED_CONNECTION=YES; "
        conn_str3="SERVER="+str(self.dbName)+"; "
        conn_str4=" DATABASE=AdventureWorks2012;"
        conn_str5=" UID="+str(self.uName)+"; "
        conn_str6=" PWD="+str(self.password)+";"
        conn_str=conn_str1+conn_str2+conn_str3+conn_str4+conn_str5+conn_str6
        try:
            conn=pypyodbc.connect(conn_str)
        except pypyodbc.DatabaseError:
            print("Connection to Database has Failed!!")
        cursor = conn.cursor()
        cursor.execute('''DROP TABLE TestDB''')
        cursor.commit()
        command = '''SELECT FirstName, LastName INTO TestDB FROM Person.Person;'''
        cursor.execute(command)
        cursor.commit()
        cursor.execute("ALTER TABLE TestDB ADD IsUpdated varchar(10) NOT NULL DEFAULT 'No'")
        cursor.commit()
        cursor.execute("ALTER TABLE TestDB ADD ID int IDENTITY(1,1) PRIMARY KEY NOT NULL;")
        cursor.commit()
        cursor.close()
        conn.close()
